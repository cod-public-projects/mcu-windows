import {
  arraySplit, matchFeature,
  getRandomString, getBitWithNumber,
  significantBytesToNumber, numberToSignificantBytes,
  mapNumber,
} from '@/script/utils/utils';

describe('arraySplit', () => {
  it('空矩陣', () => {
    const array = [];
    const result = arraySplit(array, 10);

    expect(result.length).toBe(1);
  });

  it('separator 不存在矩陣內', () => {
    const array = [10, 1, 2, 3, 4, 10, 5, 6, 10, 7, 8];
    const result = arraySplit(array, 30);

    expect(result.length).toBe(1);
  });

  it('例子 1', () => {
    const array = [10, 1, 2, 10, 5, 6, 10, 7];
    const result = arraySplit(array, 10);

    expect(result.length).toBe(4);
    expect(result[0]).toEqual([]);
    expect(result[1]).toEqual([1, 2]);
    expect(result[2]).toEqual([5, 6]);
    expect(result[3]).toEqual([7]);
  });

  it('例子 2', () => {
    const array = [10, 10, 10, 10];
    const result = arraySplit(array, 10);

    expect(result.length).toBe(5);
  });

  it('例子 3', () => {
    const array = [1, 2, 10, 3, 4];
    const result = arraySplit(array, 10);

    expect(result.length).toBe(2);
    expect(result[0]).toEqual([1, 2]);
    expect(result[1]).toEqual([3, 4]);
  });

  it('例子 4', () => {
    const array = [1, 2, 10, 10, 3, 4];
    const result = arraySplit(array, 10);

    expect(result.length).toBe(3);
    expect(result[0]).toEqual([1, 2]);
    expect(result[1]).toEqual([]);
    expect(result[2]).toEqual([3, 4]);
  });
});


describe('matchFeature', () => {
  const array = [1, 2, 3, 4, 5, 6];

  it('case 1', () => {
    const feature = [];
    const result = matchFeature(array, feature);

    expect(result).toBe(false);
  });

  it('case 2', () => {
    const feature = [1, 3];
    const result = matchFeature(array, feature);

    expect(result).toBe(false);
  });

  it('case 3', () => {
    const feature = [1, 2, 3, 4, 5, 6, 7, 8];
    const result = matchFeature(array, feature);

    expect(result).toBe(false);
  });

  it('case 4', () => {
    const feature = [7, 8];
    const result = matchFeature(array, feature);

    expect(result).toBe(false);
  });

  it('case 5', () => {
    const feature = [1, 2];
    const result = matchFeature(array, feature);

    expect(result).toBe(true);
  });

  it('case 6', () => {
    const feature = [1, 2, 3, 4, 5, 6];
    const result = matchFeature(array, feature);

    expect(result).toBe(true);
  });
});


describe('getRandomString', () => {
  it('無任何參數', () => {
    const string = getRandomString();
    expect(string.length).toBe(5);
  });

  it('長度 10', () => {
    const string = getRandomString(10);
    expect(string.length).toBe(10);
  });

  it('指定字元', () => {
    const charSet = 'ABCDEFG';
    const string = getRandomString(10, charSet);

    const hasChar = string.split('').some((char) =>
      charSet.includes(char)
    );

    expect(string.includes('H')).toBe(false);
    expect(hasChar).toBe(true);
  });
});

describe('significantBytesToNumber', () => {
  it('[1, 0]', () => {
    const array = [1, 0];
    const result = significantBytesToNumber(array);

    expect(result).toBe(1);
  });

  it('[0, 1]', () => {
    const array = [0, 1];
    const result = significantBytesToNumber(array);

    expect(result).toBe(128);
  });

  it('[8, 1]', () => {
    const array = [8, 1];
    const result = significantBytesToNumber(array);

    expect(result).toBe(136);
  });

  it('[127, 1]', () => {
    const array = [127, 1];
    const result = significantBytesToNumber(array);

    expect(result).toBe(255);
  });
});

describe('numberToSignificantBytes', () => {
  it('0', () => {
    const value = 0;
    const result = numberToSignificantBytes(value);

    expect(result.length).toBe(2);
    expect(result[0]).toBe(0);
    expect(result[1]).toBe(0);
  });

  it('0', () => {
    const value = 0;
    const result = numberToSignificantBytes(value, 1);

    expect(result.length).toBe(1);
    expect(result[0]).toBe(0);
  });

  it('100', () => {
    const value = 100;
    const result = numberToSignificantBytes(value);

    expect(result.length).toBe(2);
    expect(result[0]).toBe(100);
    expect(result[1]).toBe(0);
  });

  it('200', () => {
    const value = 200;
    const result = numberToSignificantBytes(value);

    expect(result.length).toBe(2);
    expect(result[0]).toBe(72);
    expect(result[1]).toBe(1);
  });

  it('255', () => {
    const value = 255;
    const result = numberToSignificantBytes(value);

    expect(result.length).toBe(2);
    expect(result[0]).toBe(127);
    expect(result[1]).toBe(1);
  });

  it('400', () => {
    const value = 400;
    const result = numberToSignificantBytes(value);

    expect(result.length).toBe(2);
    expect(result[0]).toBe(16);
    expect(result[1]).toBe(3);
  });
});

describe('getBitWithNumber', () => {
  it('取得 128 第 7 bit', () => {
    const result = getBitWithNumber(128, 7);
    expect(result).toBe(true);
  });
  it('取得 128 第 6 bit', () => {
    const result = getBitWithNumber(128, 6);
    expect(result).toBe(false);
  });
  it('取得 128 第 8 bit', () => {
    const result = getBitWithNumber(128, 8);
    expect(result).toBe(false);
  });

  it('取得 8 第 3 bit', () => {
    const result = getBitWithNumber(8, 3);
    expect(result).toBe(true);
  });
  it('取得 8 第 2 bit', () => {
    const result = getBitWithNumber(8, 2);
    expect(result).toBe(false);
  });
  it('取得 8 第 4 bit', () => {
    const result = getBitWithNumber(8, 4);
    expect(result).toBe(false);
  });
});

describe('mapNumber', () => {
  it('低於範圍', () => {
    const result = mapNumber(10, 50, 100, 500, 1000);
    expect(result).toBe(500);
  });

  it('高於範圍', () => {
    const result = mapNumber(200, 50, 100, 500, 1000);
    expect(result).toBe(1000);
  });

  it('75 映射至 750', () => {
    const result = mapNumber(75, 50, 100, 500, 1000);
    expect(result).toBe(750);
  });

  it('60 映射至 600', () => {
    const result = mapNumber(60, 50, 100, 500, 1000);
    expect(result).toBe(600);
  });

  it('99 映射至 990', () => {
    const result = mapNumber(99, 50, 100, 500, 1000);
    expect(result).toBe(990);
  });
});