const WindiCSS = require('windicss-webpack-plugin').default;

module.exports = {
  configureWebpack: {
    plugins: [
      new WindiCSS(),
    ],
  },
  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: false,
    },
    i18n: {
      locale: 'tw',
      fallbackLocale: 'tw',
      localeDir: 'locales',
      enableInSFC: false,
    },
  },
  transpileDependencies: [
    'quasar',
  ],
};
