# MCU Windows

2021 年第 13 屆鐵人賽參賽專案

# 說明

隨著瀏覽器支援的 Web API 越來越豐富，有一天我注意到了「Web Serial API」這個神奇的東東。

以往 JS 沒有權限能夠存取作業系統底層 API，所以要做串列通訊都需要一個中介伺服器轉送資料，但是透過 Web Serial API 就可以直接透過瀏覽器進行串列通訊了！於是這個主題就這麼誕生了。

接下來的 30 天，我將嘗試透過 Web Serial API 與單晶片直接連結並將電訊號轉換成 UI 展示，最後在網頁上開發遊戲，透過實體電路的按鈕、搖桿遊玩！

以下是目前功能視窗簡介：

- 「數位 I/O」視窗
    
    可以進行數位輸入輸入。
    

- 「類比輸入」視窗
    
    取得類比電壓輸入。
    

- 「PWM」視窗
    
    PWM 輸出。
    

- 「調色盤」視窗
    
    透過 PWM 輸出，達成 RGB LED 調色功能。
    

- 「跑跑小恐龍」視窗
    
    重現 Google Dino 遊戲，透過指定兩個數位輸入按鈕，進行跳躍、蹲下。
    
- 「貓狗大戰」視窗
    
    透過搖桿控制人物移動、發射武器。
    

技術關鍵字：
Firmata Protocol、Web Serial API、Vue、Vuex、Quasar、GSAP、Phaser

系列文章連結：

[https://ithelp.ithome.com.tw/users/20140213/ironman/4765](https://ithelp.ithome.com.tw/users/20140213/ironman/4765)

成果影片：

[https://www.youtube.com/watch?v=OpayalfQ124](https://www.youtube.com/watch?v=OpayalfQ124)