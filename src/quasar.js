import Vue from 'vue';

import './styles/quasar.sass';
import lang from 'quasar/lang/zh-hant.js';
import '@quasar/extras/roboto-font/roboto-font.css';
import '@quasar/extras/material-icons-round/material-icons-round.css';

import { Quasar, Notify, QSeparator } from 'quasar';
import iconSet from 'quasar/icon-set/material-icons-round';

Vue.use(Quasar, {
  config: {
    notify: {
      position: 'top',
      timeout: 3000,
    },
  },
  components: {
    QSeparator,
  },
  directives: {
  },
  plugins: {
    Notify,
  },
  lang,
  iconSet
});
