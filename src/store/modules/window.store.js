/**
 * 管理視窗相關資料
 */

/**
 * @typedef {import('vuex').Module} Module
 * 
 * @typedef {import('@/types/type').Window} Window
 * @typedef {import('@/types/type').OccupiedPin} OccupiedPin
 */

import { cloneDeep } from 'lodash-es';
import dayjs from 'dayjs';

import { getRandomString } from '@/script/utils/utils';

/** @type {Module} */
const self = {
  namespaced: true,
  state: () => ({
    /** @type {Window[]} */
    list: [],

    focusId: null,
  }),
  mutations: {
    /** 新增視窗 */
    add(state, component) {
      /** @type {Window} */
      const window = {
        component,
        id: getRandomString(),
        focusAt: dayjs().valueOf(),
        occupiedPins: [],
      }

      state.list.push(window);
    },
    /** 刪除視窗 */
    remove(state, id) {
      /** @type {Window[]} */
      const windows = state.list;

      const targetIndex = windows.findIndex((window) =>
        window.id === id
      );
      if (targetIndex < 0) {
        console.error(`[ window.store remove ] window 不存在，id : `, id);
        return;
      }

      windows.splice(targetIndex, 1);
    },

    /** 設目前 Focus 視窗 */
    setFocus(state, id) {
      state.focusId = id;

      /** @type {Window[]} */
      const windows = state.list;

      const target = windows.find((window) =>
        window.id === id
      );
      if (!target) {
        return;
      }

      target.focusAt = dayjs().valueOf();
    },

    /** Window 新增占用腳位 */
    addOccupiedPin(state, { id, pin }) {
      /** @type {Window[]} */
      const windows = state.list;

      const target = windows.find((window) => window.id === id);
      if (!target) {
        console.error(`[ window.store addOccupiedPin ] window 不存在，id : `, id);
        return;
      }

      target.occupiedPins.push(pin);
    },

    /** Window 移除占用腳位 */
    deleteOccupiedPin(state, { id, pin }) {
      /** @type {Window[]} */
      const windows = state.list;

      const target = windows.find((window) => window.id === id);
      if (!target) {
        console.error(`[ window.store deleteOccupiedPin ] window 不存在，id : `, id);
        return;
      }

      const targetPinIndex = target.occupiedPins.findIndex(({ number }) =>
        number === pin.number
      );
      if (targetPinIndex < 0) {
        return;
      }

      target.occupiedPins.splice(targetPinIndex, 1);
    },
  },
  actions: {
  },
  getters: {
    /** Window 對應的 z-index
     * 
     * 視窗 ID 與 z-index 以 key-value 對應
     * @example
     * map['abcds']: 1
     * map['gr56w']: 2
     */
    zIndexMap: (state) => {
      /** @type {Window[]} */
      const windows = cloneDeep(state.list);

      windows.sort((a, b) => a.focusAt > b.focusAt ? 1 : -1);

      return windows.reduce((map, window, index) => {
        map[window.id] = index;
        return map;
      }, {});
    },

    /** 列出所有被占用的腳位
     * @return {OccupiedPin[]}
     */
    occupiedPins: (state) => {
      /** @type {Window[]} */
      const windows = state.list;

      // 找出有占用腳位的 window
      const occupiedPinWindows = windows.filter(({ occupiedPins }) =>
        occupiedPins.length !== 0
      );

      const occupiedPins = occupiedPinWindows.reduce((acc, window) => {
        const { component, id } = window;

        window.occupiedPins.forEach((pin) => {

          acc.push({
            info: pin,
            occupier: {
              component, id,
            },
          });
        });

        return acc;
      }, []);

      return occupiedPins;
    },
  },
  modules: {
  },
};

export default self;
