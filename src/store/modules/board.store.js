/**
 * 管理 Firmata 版本、Pin 清單等等 MCU 開發版相關資料
 */

/**
 * @typedef {import('vuex').Module} Module
 */

/** @type {Module} */
const self = {
  namespaced: true,
  state: () => ({
    info: {
      ver: null,
      firmwareName: null,
      pins: [],
      analogPinMap: {},
    },

  }),
  mutations: {
    setInfo(state, info) {
      state.info = {
        ...state.info, ...info
      }
    },
  },
  actions: {
  },
  modules: {
  },
};

export default self;
