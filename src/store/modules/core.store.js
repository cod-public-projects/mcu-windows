/**
 * 管理 Port 物件、系統主要設定
 */

/**
 * @typedef {import('vuex').Module} Module
 */

import PortTransceiver from '@/script/modules/port-transceiver';

/** @type {Module} */
const self = {
  namespaced: true,
  state: () => ({
    port: null,

    /** @type {PortTransceiver} */
    transceiver: null,
  }),
  mutations: {
    setPort(state, port) {
      state.transceiver?.stop?.();
      state.port = port;

      if (!port) {
        state.transceiver = null;
        return;
      }

      state.transceiver = new PortTransceiver(port);
    },
  },
  actions: {
  },
  modules: {
  },
};

export default self;