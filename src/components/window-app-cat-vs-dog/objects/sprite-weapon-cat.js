import Phaser from 'phaser';

/**
 * @typedef {Object} ConstructorParams
 * @property {number} [x]
 * @property {number} [y]
 */

export default class extends Phaser.Physics.Arcade.Sprite {
  /**
   * @param {Phaser.Scene} scene 
   * @param {ConstructorParams} params
   */
  constructor(scene, params) {
    const { x = 0, y = 0 } = params;

    super(scene, x, y, 'cat-weapon');
    this.scene = scene;
  }

  preUpdate(time, delta) {
    super.preUpdate(time, delta);

    /** 檢查武器是否超出世界邊界
     * 透過偵測武器是否與世界有碰撞，取反向邏輯
     * 沒有碰撞，表示物體已經超出邊界
     */
    const outOfBoundary = !Phaser.Geom.Rectangle.Overlaps(
      this.scene.physics.world.bounds,
      this.getBounds(),
    );

    // 隱藏超出邊界武器並關閉活動
    if (outOfBoundary) {
      this.setActive(false)
        .setVisible(false);
    }
  }

  /** 發射武器
   * @param {number} x 
   * @param {number} y 
   * @param {number} velocity 
   */
  fire(x, y, velocity) {
    // 清除所有加速度、速度並設置於指定座標
    this.body.reset(x, y);

    // 角速度
    const angularVelocity = Phaser.Math.Between(-400, 400);

    this.setScale(0.3)
      .setSize(160, 160)
      .setAngularVelocity(angularVelocity)
      .setVelocityY(velocity)
      .setActive(true)
      .setVisible(true);
  }
}