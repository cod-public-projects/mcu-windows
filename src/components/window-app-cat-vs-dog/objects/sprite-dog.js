/**
 * @typedef {import('@/components/window-app-cat-vs-dog/objects/group-weapon')} GroupWeapon
 * 
 * @typedef {Object} DogParams
 * @property {number} [x]
 * @property {number} [y]
 * @property {Phaser.Physics.Arcade.Sprite} target
 * @property {GroupWeapon} weapon
 */

import Phaser from 'phaser';

import { delay } from '@/script/utils/utils';

export default class extends Phaser.Physics.Arcade.Sprite {
  /** @type {Phaser.Physics.Arcade.Sprite} */
  target = null;

  /** @type {GroupWeapon} */
  weapon = null;
  health = 10;

  /**
   * @param {Phaser.Scene} scene 
   * @param {DogParams} params
   */
  constructor(scene, params) {
    const {
      x = 500, y = 600,
      weapon = null,
      target = null,
    } = params;

    if (!weapon) throw new Error('weapon 為必填參數');

    super(scene, x, y, 'dog-work');
    scene.add.existing(this);
    scene.physics.add.existing(this);

    this.setScale(0.2)
      .setSize(340, 420)
      .setCollideWorldBounds(true);
    this.play('dog-work');

    this.scene = scene;
    this.target = target;
    this.weapon = weapon;

    this.initAutomata();
  }

  preUpdate(time, delta) {
    super.preUpdate(time, delta);

    if (!this.anims.isPlaying) {
      this.play('dog-work');
    }
  }

  /** 取得生命值 */
  getHealth() {
    return this.health;
  }
  /** 扣血 */
  subHealth(val = 1) {
    this.health = Phaser.Math.MinSub(this.health, val, 0);
    this.play('dog-beaten', true);

    if (this.health === 0) {
      this.emit('death');
    }
  }

  initAutomata() {
    // 隨機發射
    this.scene.time.addEvent({
      delay: 500,
      callbackScope: this,
      repeat: -1,
      callback: async () => {
        await delay(Phaser.Math.Between(0, 200));
        this.fire();
      },
    });

    // 追貓
    this.scene.time.addEvent({
      delay: 500,
      callbackScope: this,
      repeat: -1,
      callback: async () => {
        const vx = (this.target.x - this.x) * 1.5;
        const vy = Phaser.Math.Between(-400, 400);

        this.setVelocity(vx, vy);
      },
    });
  }

  fire() {
    this.weapon.fire(this.x, this.y, -500);
    this.play('dog-attack', true);
  }
}