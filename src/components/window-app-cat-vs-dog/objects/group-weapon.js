import Phaser from 'phaser';

/**
 * @typedef {Object} ConstructorParams
 * @property {Object} classType 注入武器物件
 * @property {string} key
 * @property {number} quantity 物體數量
 */

export default class extends Phaser.Physics.Arcade.Group {
  /**
   * @param {Phaser.Scene} scene 
   * @param {ConstructorParams} params 
   */
  constructor(scene, params) {
    super(scene.physics.world, scene);

    const { classType, key, quantity = 5 } = params;

    this.createMultiple({
      classType,
      frameQuantity: quantity,
      active: false,
      visible: false,
      key,
    });

    this.setDepth(1);

    // 隱藏所有武器
    this.getChildren().forEach((item) => {
      item.setScale(0);
    });
  }

  /** 發射武器
   * @param {number} x 
   * @param {number} y 
   * @param {number} velocity 
   */
  fire(x, y, velocity) {
    const weapon = this.getFirstDead(false);
    if (weapon) {
      weapon.body.enable = true;
      weapon.fire(x, y, velocity);
    }
  }
}