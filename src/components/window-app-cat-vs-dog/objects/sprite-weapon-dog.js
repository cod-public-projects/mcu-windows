import Phaser from 'phaser';

export default class extends Phaser.Physics.Arcade.Sprite {
  /**
   * @param {Phaser.Scene} scene 
   * @param {number} x 
   * @param {number} y 
   */
  constructor(scene, x = 0, y = 0) {
    super(scene, x, y, 'dog-weapon');
    this.scene = scene;
  }

  preUpdate(time, delta) {
    super.preUpdate(time, delta);

    const outOfBoundary = !Phaser.Geom.Rectangle.Overlaps(
      this.scene.physics.world.bounds,
      this.getBounds(),
    );

    if (outOfBoundary) {
      this.setActive(false);
      this.setVisible(false);
    }
  }

  /** 發射武器
   * @param {number} x 
   * @param {number} y 
   * @param {number} velocity 
   */
  fire(x, y, velocity) {
    this.body.reset(x, y);

    const angularVelocity = Phaser.Math.Between(-400, 400);

    this.setScale(0.2)
      .setSize(300, 300)
      .setAngularVelocity(angularVelocity)
      .setVelocityY(velocity)
      .setActive(true)
      .setVisible(true);
  }
}