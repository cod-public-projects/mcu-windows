/**
 * @typedef {import('@/script/electronic-components/joy-stick').default} JoyStick
 * @typedef {import('@/components/window-app-cat-vs-dog/objects/group-weapon')} GroupWeapon
 * 
 * @typedef {Object} CatParams
 * @property {number} [x]
 * @property {number} [y]
 * @property {GroupWeapon} weapon
 */

import Phaser from 'phaser';

/** 最大速度 */
const velocityMax = 300;

export default class extends Phaser.Physics.Arcade.Sprite {
  weapon = null;

  /** 血量 */
  health = 5;

  /**
   * @param {Phaser.Scene} scene 
   * @param {CatParams} params
   */
  constructor(scene, params = {}) {
    const {
      x = 200, y = 200,
      weapon = null,
    } = params;

    if (!weapon) throw new Error('weapon 為必填參數');

    super(scene, x, y, 'cat-work');

    // 將人物加入至場景並加入物理系統
    scene.add.existing(this);
    scene.physics.add.existing(this);

    // 設定人物縮放、尺寸、碰撞反彈、世界邊界碰撞
    this.setScale(0.3)
      .setSize(220, 210)
      .setBounce(0.2)
      .setCollideWorldBounds(true);

    // 播放動畫
    this.play('cat-work');

    this.scene = scene;
    this.weapon = weapon;


    /** @type {JoyStick} */
    const joyStick = scene.game.joyStick;

    joyStick.on('data', ({ x, y }) => {
      // 將 x、y 數值組合為向量並轉為單位向量。
      const velocityVector = new Phaser.Math.Vector2(x, y);
      velocityVector.normalize();

      // 將單位向量 x、y 分量分別乘上最大速度
      const { x: vx, y: vy } = velocityVector;
      this.setVelocity(vx * velocityMax, vy * velocityMax);
    }).on('rising', () => {
      this.weapon.fire(this.x, this.y, 800);
      this.play('cat-attack', true);
      this.setVelocity(0, 0);
    });


    this.once('destroy', () => {
      joyStick.removeAllListeners();
    });
  }

  preUpdate(time, delta) {
    super.preUpdate(time, delta);

    if (!this.anims.isPlaying) {
      this.play('cat-work');
    }
  }

  /** 取得生命值 */
  getHealth() {
    return this.health;
  }
  /** 扣血 */
  subHealth(val = 1) {
    this.health = Phaser.Math.MinSub(this.health, val, 0);
    this.play('cat-beaten', true);

    if (this.health === 0) {
      this.emit('death');
    }
  }
}