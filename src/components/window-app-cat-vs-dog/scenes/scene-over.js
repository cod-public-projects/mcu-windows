import Phaser from 'phaser';

export default class extends Phaser.Scene {
  constructor() {
    super({ key: 'over' })
  }
  create(result) {
    const x = this.game.config.width / 2;
    const y = this.game.config.height / 2;

    const text = result === 'win' ? '恭喜獲勝' : '哭哭，被打敗了';
    const texture = result === 'win' ? 'cat-attack' : 'cat-beaten';

    // 主角
    this.cat = this.physics.add.sprite(x, y - 80, texture)
      .setScale(0.5);

    // 提示文字
    this.add.text(x, y + 50, text, {
      fill: '#000',
      fontSize: 30,
    }).setOrigin(0.5);

    this.add.text(x, y + 100, '按下搖桿按鍵重新開始', {
      fill: '#000',
      fontSize: 18,
    }).setOrigin(0.5);


    /** @type {JoyStick} */
    const joyStick = this.game.joyStick

    // 延遲一秒鐘後再偵測搖桿按鈕，才不會一進到場景後誤按按鈕馬上觸發
    setTimeout(() => {
      joyStick.once('toggle', () => {
        this.scene.start('main');
      });
    }, 1000);
  }
}
