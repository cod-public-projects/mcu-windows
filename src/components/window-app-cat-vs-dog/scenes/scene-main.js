import Phaser from 'phaser';

import SpriteCat from '@/components/window-app-cat-vs-dog/objects/sprite-cat';
import SpriteDog from '@/components/window-app-cat-vs-dog/objects/sprite-dog';

import GroupWeapon from '@/components/window-app-cat-vs-dog/objects/group-weapon';
import SpriteWeaponCat from '@/components/window-app-cat-vs-dog/objects/sprite-weapon-cat';
import SpriteWeaponDog from '@/components/window-app-cat-vs-dog/objects/sprite-weapon-dog';

export default class extends Phaser.Scene {
  constructor() {
    super({ key: 'main' })
  }
  create() {
    // 主角
    const catWeapon = new GroupWeapon(this, {
      classType: SpriteWeaponCat,
      key: 'cat-weapon',
      quantity: 1
    });
    this.cat = new SpriteCat(this, {
      weapon: catWeapon,
    });


    // 敵人
    const dogWeapon = new GroupWeapon(this, {
      classType: SpriteWeaponDog,
      key: 'dog-weapon',
    });
    this.dog = new SpriteDog(this, {
      weapon: dogWeapon,
      target: this.cat,
    });


    // 顯示生命值
    this.catHealthText = this.add.text(20, 20, `貓命：${this.cat.health}`, {
      fill: '#000',
      fontSize: 14,
    });

    const sceneHeight = this.game.config.height;
    this.dogHealthText = this.add.text(20, sceneHeight - 20, `狗血：${this.dog.health}`, {
      fill: '#000',
      fontSize: 14,
    }).setOrigin(0, 1);


    // 加入中央河流
    this.platforms = this.physics.add.staticGroup();
    this.platforms.create(300, 400, 'river').setScale(0.17).refreshBody();

    // 加入河流與人物碰撞
    this.physics.add.collider([this.cat, this.dog, this.platforms]);

    // 加入武器與人物碰撞
    this.physics.add.overlap(this.cat, dogWeapon, (cat, weapon) => {
      // 隱藏武器
      weapon.body.enable = false;
      weapon.setActive(false).setVisible(false);

      // 主角扣血
      this.cat.subHealth();
    });

    this.physics.add.overlap(this.dog, catWeapon, (dog, weapon) => {
      // 隱藏武器
      weapon.body.enable = false;
      weapon.setActive(false).setVisible(false);

      // 敵人扣血
      this.dog.subHealth();
    });


    // 偵測人物事件
    this.dog.once('death', () => {
      this.scene.start('over', 'win');
    });
    this.cat.once('death', () => {
      this.scene.start('over', 'lose');
    });
  }
  update() {
    this.catHealthText.setText(`貓命：${this.cat.health}`);
    this.dogHealthText.setText(`狗血：${this.dog.health}`);
  }
}
