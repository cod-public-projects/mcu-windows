/**
 * @typedef {import('@/script/electronic-components/joy-stick').default} JoyStick
 */

import Phaser from 'phaser';

export default class extends Phaser.Scene {
  constructor() {
    super({ key: 'welcome' })
  }
  create() {
    const x = this.game.config.width / 2;
    const y = this.game.config.height / 2;

    this.cat = this.physics.add
      .sprite(x, y - 80, 'cat-work')
      .setScale(0.5)
      .setCollideWorldBounds(true)
      .anims.play('cat-work');

    this.add.text(x, y + 50, '按下搖桿按鍵開始', {
      fill: '#000',
      fontSize: 30,
    }).setOrigin(0.5);


    /** @type {JoyStick} */
    const joyStick = this.game.joyStick
    joyStick.on('data', ({ x, y }) => {
      this.cat.setVelocity(x, y);
    }).once('toggle', () => {
      this.scene.start('main');
    });

    /** 監聽 destroy 事件，清除所有搖桿監聽器
     * 以免人物被銷毀後，搖桿還持續呼叫 setVelocity，導致錯誤
     */
    this.cat.once('destroy', () => {
      joyStick.removeAllListeners();
    });
  }
}
