/**
 * @typedef {import('@/script/firmata/firmata').PinInfo} PinInfo
 * @typedef {import('@/script/firmata/firmata').PinCapability} PinCapability
 * @typedef {import('@/script/firmata/firmata').DigitalResponseMessage} DigitalResponseMessage
 * @typedef {import('@/script/firmata/firmata').AnalogResponseMessage} AnalogResponseMessage
 * 
 * @typedef {Object} Window 已建立視窗
 * @property {String} component 組件名稱
 * @property {String} id 視窗 ID
 * @property {Number} focusAt 聚焦時間
 * @property {PinInfo[]} occupiedPins 已占用腳位
 * 
 * @typedef {Object} OccupiedPin 被占用的腳位
 * @property {PinInfo} info 腳位資訊
 * @property {Object} occupier 占用者
 * @property {String} occupier.component 組件名稱
 * @property {String} occupier.id 視窗 ID
 */

exports.unused = {};
