/**
 * @typedef {import('EventEmitter2').Listener} Listener
 * 
 * @typedef {import('@/script/modules/port-transceiver').default} PortTransceiver
 *
 * @typedef {import('@/types/type').PinInfo} PinInfo
 * @typedef {import('@/script/firmata/firmata').DigitalResponseMessage} DigitalResponseMessage
 * @typedef {import('@/script/firmata/firmata').AnalogResponseMessage} AnalogResponseMessage
 * @typedef {import('@/script/electronic-components/Button').ConstructorParams} ButtonParams
 *
 * @typedef {Object} ConstructorParams
 * @property {PortTransceiver} transceiver Port 收發器
 * @property {Object} analogPinMap 類比腳位映射表
 * @property {AxisOptions} xAxis X 軸設定
 * @property {AxisOptions} yAxis Y 軸設定
 * @property {Object} btn 按鈕設定
 * @property {PinInfo} btn.pin 指定腳位
 * @property {number} btn.mode 按鈕腳位模式
 * 
 * @typedef {Object} AxisOptions 軸設定
 * @property {PinInfo} pin 指定腳位
 * @property {number} [origin] 原點。搖桿無動作時，類比數值基準點。
 * @property {number} [threshold] 閥值。origin 正負 threshold 之內的數值，視為沒有動作。
 * @property {boolean} [isReverse] 軸反轉
 * 
 */

import EventEmitter2 from 'eventemitter2';
import { findLast, throttle } from 'lodash-es';
// import utils from '@/script/utils/utils';
// const { delay } = utils;

import { PinMode } from '@/script/utils/firmata.utils';

import Button from './button';

const axisOptionsDefault = {
  origin: 510,
  threshold: 20,
  isReverse: false,
};

/** 
 * 基本按鈕
 * 支援數位輸入、上拉輸入
 */
export default class extends EventEmitter2.EventEmitter2 {
  /** 目前軸類比數值 */
  axesValue = {
    x: 0,
    y: 0,
  };

  /** @type {PortTransceiver} */
  portTransceiver = null;
  analogPinMap = {};

  /** X 軸設定
   * @type {AxisOptions}
   */
  xAxis = null;
  /** Y 軸設定
   * @type {AxisOptions}
   */
  yAxis = null;

  /** 按鈕物件
   * @type {Button}
   */
  btn = null;

  /** 數值回報監聽器
   * @type {Listener}
   */
  listener = null;

  throttle = {
    setAxesValue: null,
  };

  /**
   * @param {ConstructorParams} params
   */
  constructor(params) {
    super();

    const {
      transceiver = null,
      analogPinMap = null,
      xAxis, yAxis, btn
    } = params;

    if (!transceiver) throw new Error(`transceiver 必填`);
    if (!analogPinMap) throw new Error(`analogPinMap 必填`);
    if (!xAxis?.pin) throw new Error(`xAxis.pin 必填`);
    if (!yAxis?.pin) throw new Error(`yAxis.pin 必填`);
    if (!btn?.pin) throw new Error(`btn.pin 必填`);

    // 儲存變數
    this.portTransceiver = transceiver;
    this.analogPinMap = analogPinMap;
    this.xAxis = {
      ...axisOptionsDefault, ...xAxis
    }
    this.yAxis = {
      ...axisOptionsDefault, ...yAxis
    }

    /** 初始化按鈕物件 */
    this.btn = new Button({
      ...btn,
      transceiver,
    });
    /** 將所有 btn 事件轉送出去 */
    this.btn.onAny((event, value) => this.emit(event, value));

    /** 建立 throttle 功能 */
    this.throttle = {
      setAxesValue: throttle((...params) => {
        this.setAxesValue(...params);
      }, 100),
    }

    this.init();
  }

  /** 初始化
   * 進行腳位設定、啟動監聽器
   */
  async init() {
    const xPinNum = this.xAxis.pin.number;
    const yPinNum = this.yAxis.pin.number;

    this.portTransceiver.addCmd('setMode', {
      pin: xPinNum,
      mode: PinMode.ANALOG_INPUT,
    });
    this.portTransceiver.addCmd('setMode', {
      pin: yPinNum,
      mode: PinMode.ANALOG_INPUT,
    });

    this.listener = this.portTransceiver.on('data:analogMessage', (data) => {
      this.handleData(data);
    }, { objectify: true });
  }
  /** 銷毀所有物件、監聽器 */
  destroy() {
    this.btn.destroy();
    this.listener.off();
    this.removeAllListeners();
  }

  /** 處理類比訊號數值
   * @param {AnalogResponseMessage[]} data
   */
  handleData(data) {
    const { xAxis, yAxis, analogPinMap } = this;

    let x = 0;
    let y = 0;

    /** 取得 X 軸類比資料 */
    const xVal = findLast(data, ({ analogPin }) => {
      const mapNum = analogPinMap[xAxis.pin.number];
      return mapNum === analogPin
    });
    if (xVal) {
      x = this.calcAxisValue(xVal.value, xAxis)
    }

    /** 取得 Y 軸類比資料 */
    const yVal = findLast(data, ({ analogPin }) => {
      const mapNum = analogPinMap[yAxis.pin.number];
      return mapNum === analogPin
    });
    if (yVal) {
      y = this.calcAxisValue(yVal.value, yAxis)
    }

    this.throttle.setAxesValue({
      x, y,
    });
  }

  /** 儲存軸向類比數值 */
  setAxesValue({ x, y }) {
    this.axesValue.x = x;
    this.axesValue.y = y;

    this.emit('data', this.axesValue);
  }
  /** 取得軸向類比數值 */
  getAxesValue() {
    return this.axesValue;
  }
  /** 取得按鈕數值 */
  getBtnValue() {
    return this.btn.getValue();
  }

  /** 將類比數值轉換為搖桿軸資料
   * @param {number} value
   * @param {AxisOptions} options 
   */
  calcAxisValue(value, options) {
    const { origin, threshold, isReverse } = options;

    /** 
     * 需要設定 threshold，是因為實際上搖桿回到中點時，
     * 類比數值並非完全靜止，可能會在正負 1 或 2 些微浮動，
     * 如果判斷搖桿靜止是用單一數值判斷，容易誤判，
     * 所以透過 threshold，以範圍來判斷，就可以解決誤判問題。
     */
    const delta = origin - value;
    if (Math.abs(delta) < threshold) {
      return 0;
    }

    return isReverse ? delta * -1 : delta;
  }

}
