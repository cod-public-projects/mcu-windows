/**
 * @typedef {import('@/script/modules/port-transceiver').default} PortTransceiver
 * @typedef {import('eventemitter2').Listener} Listener
 *
 * @typedef {import('@/types/type').PinInfo} PinInfo
 * @typedef {import('@/script/firmata/firmata').DigitalResponseMessage} DigitalResponseMessage
 * 
 * @typedef {Object} ConstructorParams
 * @property {PinInfo} pin 
 * @property {number} mode 腳位模式
 * @property {PortTransceiver} transceiver
 * @property {Number} [debounceMillisecond] 去彈跳時長
 */

import EventEmitter2 from 'eventemitter2';
import { findLast, debounce } from 'lodash-es';
import { delay, getBitWithNumber } from '@/script/utils/utils';

import { PinMode } from '@/script/utils/firmata.utils';
const { DIGITAL_INPUT, INPUT_PULLUP } = PinMode;

/** 
 * 基本按鈕
 * 支援數位輸入、上拉輸入
 */
export default class extends EventEmitter2.EventEmitter2 {
  /** 指定腳位 
   * @type {PinInfo}
   */
  pin = null;

  /** 腳位 Port 號 */
  portNum = 0;

  /** 腳位模式 */
  mode = 0;

  /** COM Port 收發器 
   * @type {PortTransceiver}
   */
  portTransceiver = null;

  /** 訊號回報監聽器 
   * @type {Listener}
   */
  listener = null;

  /** 目前數位訊號 */
  value = false;


  /**
   * @param {ConstructorParams} params
   */
  constructor(params) {
    super();

    const {
      pin = null,
      transceiver = null,
      mode = DIGITAL_INPUT,
      debounceMillisecond = 10,
    } = params;

    if (!pin) throw new Error(`pin 必填`);
    if (!transceiver) throw new Error(`transceiver 必填`);
    if (![DIGITAL_INPUT, INPUT_PULLUP].includes(mode)) {
      throw new Error(`不支援指定的腳位模式 : ${mode}`);
    }

    this.pin = pin;
    this.portNum = (pin.number / 8) | 0;
    this.mode = mode;
    this.portTransceiver = transceiver;


    this.options = {
      debounceMillisecond
    }
    this.debounce = {
      prcoessEvent: debounce((...params) => {
        this.prcoessEvent(...params)
      }, debounceMillisecond),
    }

    this.init();
  }

  async init() {
    this.portTransceiver.addCmd('setMode', {
      pin: this.pin.number,
      mode: this.mode,
    });

    // 延遲一下再監聽數值，忽略 setMode 初始化的數值變化
    await delay(500);

    this.listener = this.portTransceiver.on(
      'data:digitalMessage',
      (data) => {
        this.handleData(data);
      },
      { objectify: true }
    );
  }
  destroy() {
    // 銷毀所有監聽器，以免 Memory Leak
    this.listener?.off?.();
    this.removeAllListeners();
  }

  /** 處理數值
   * @param {DigitalResponseMessage[]} data
   */
  handleData(data) {
    const target = findLast(data, ({ port }) => this.portNum === port);
    if (!target) return;

    const { value } = target;

    /** @type {PinInfo} */
    const pin = this.pin;
    const bitIndex = pin.number % 8;

    const pinValue = getBitWithNumber(value, bitIndex);
    this.debounce.prcoessEvent(pinValue);
  }

  /** 依照數位訊號判斷按鈕事件
   * - rising：上緣，表示放開按鈕
   * - falling：下緣，表示按下按鈕
   * - toggle：訊號切換，放開、按下都觸發
   * 
   * 參考資料：
   * [訊號邊緣](https://zh.wikipedia.org/wiki/%E4%BF%A1%E5%8F%B7%E8%BE%B9%E7%BC%98)
   * 
   * @param {boolean} value 
   */
  prcoessEvent(value) {
    let correctionValue = value;

    // 若為上拉輸入，則自動反向
    if (this.mode === INPUT_PULLUP) {
      correctionValue = !correctionValue;
    }

    if (this.value === correctionValue) return;

    if (correctionValue) {
      this.emit('rising');
    }
    if (!correctionValue) {
      this.emit('falling');
    }

    this.emit('toggle', correctionValue);
    this.value = correctionValue;
  }

  getValue() {
    if (this.mode === INPUT_PULLUP) {
      return !this.value;
    }

    return this.value;
  }
}