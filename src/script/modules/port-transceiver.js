/**
 * @typedef {Object} CmdQueueItem 命令項目
 * @property {string} key 命令 key
 * @property {Object} params 命令參數
 * @property {number[]} bytes 命令數值
 */


import EventEmitter2 from 'eventemitter2';
import to from 'safe-await';
import { debounce } from 'lodash-es';

import firmata from '@/script/firmata/firmata';

export default class extends EventEmitter2.EventEmitter2 {
  port = null;

  reader = null;
  receiveBuffer = [];

  writer = null;
  writeTimer = null;
  /** @type {CmdQueueItem[]} */
  cmdsQueue = []; // 命令佇列

  options = {
    /** 命令發送最小間距（ms） */
    writeInterval: 10,

    /** Reader 完成讀取資料之 debounce 時間
     * 由於 Firmata 採樣頻率（Sampling Interval）預設是 19ms 一次
     * 所以只要設定小於 19ms 數值都行，這裡取個整數，預設為 10ms
     * 
     * [參考文件 : Firmata Sampling Interval](https://github.com/firmata/protocol/blob/master/protocol.md#sampling-interval)
     */
    readEmitDebounce: 10,
  };
  /** debounce 原理與相關資料可以參考以下連結
   * 
   * [Debounce 和 Throttle](https://ithelp.ithome.com.tw/articles/10222749)
   */
  debounce = {
    finishReceive: null,
  };

  constructor(port) {
    super();

    // 檢查是否有 open Method
    if (!port?.open) {
      throw new TypeError('無效的 Serial Port 物件');
    }

    this.port = port;

    this.debounce.finishReceive = debounce(() => {
      this.finishReceive();
    }, this.options.readEmitDebounce);

    this.start().catch((err) => {
      // console.error(`[ PortTransceiver  start ] err : `, err);
      this.emit('err', err);
    });
  }

  /** 開啟發送佇列並監聽 Port 資料 */
  async start() {
    if (!this?.port?.open) {
      return Promise.reject(new Error('Port 無法開啟'));
    }

    /** Firmata BPS 為 57600 */
    const [err] = await to(this.port.open({ baudRate: 57600 }));
    if (err) {
      return Promise.reject(err);
    }

    this.emit('opened');
    this.startReader();
    this.startWriter();
  }
  /** 關閉 Port */
  stop() {
    this.removeAllListeners();
    clearInterval(this.writeTimer);

    this.reader?.releaseLock?.();
    this.writer?.releaseLock?.();
    this.port?.close?.();
  }

  /** Serial.Reader 開始讀取資料
   * 
   * 參考資料：
   * [W3C](https://wicg.github.io/serial/#readable-attribute)
   * [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Web_Serial_API#reading_data_from_a_port)
   */
  async startReader() {
    const port = this.port;

    if (port.readable.locked) {
      return;
    }

    try {
      this.reader = port.readable.getReader();

      for (; ;) {
        const { value, done } = await this.reader.read();
        if (done) {
          break;
        }

        // console.log(`[ startReader ] value : `, value);
        this.receiveBuffer.push(...value);
        this.debounce.finishReceive();
      }
    } catch (err) {
      this.stop();
      this.emit('err', err);
    } finally {
      this.reader?.releaseLock();
    }
  }
  /** 完成接收，emit 已接收資料 */
  finishReceive() {
    // 解析回應內容
    const results = firmata.parseResponse(this.receiveBuffer);
    if (results.length === 0) {
      this.emit('undefined-response', this.receiveBuffer);
      this.receiveBuffer.length = 0;
      return;
    }

    // emit 所有解析結果 
    results.forEach(({ key, eventName, data }) => {
      // 若 key 為 firmwareName 表示剛啟動，emit ready 事件
      if (key === 'firmwareName') {
        this.emit('ready', data);
      }

      this.emit(eventName, data);
    });

    this.receiveBuffer.length = 0;
  }

  /** 取得 Serial.Writer 並開啟發送佇列
   * 
   * 參考資料：
   * [W3C](https://wicg.github.io/serial/#writable-attribute)
   */
  startWriter() {
    this.writeTimer = setInterval(() => {
      if (this.cmdsQueue.length === 0) {
        return;
      }

      this.writer = this.port.writable.getWriter();

      const cmd = this.cmdsQueue.shift();
      this.write(cmd.bytes);

      // console.log(`write : `, cmd.bytes);
    }, this.options.writeInterval);
  }
  /** 透過 Serial.Writer 發送資料 */
  async write(data) {
    // console.log(`[ write ] data : `, data);

    await this.writer.write(new Uint8Array(data));
    this.writer.releaseLock();
  }

  /** 加入發送命令
   * @param {string} cmdKey 
   * @param {Object} params 
   */
  addCmd(cmdKey, params = null) {
    const bytes = firmata.getCmdBytes(cmdKey, params);

    /** @type {CmdQueueItem} */
    const queueItem = {
      key: cmdKey,
      params,
      bytes,
    }

    // console.log(`[ addCmd ] queueItem : `, queueItem);

    this.cmdsQueue.push(queueItem);
    return queueItem;
  }
}
