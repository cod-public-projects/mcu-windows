/**
 * @typedef {import('@/types/type').DigitalResponseMessage} DigitalResponseMessage
 * @typedef {import('@/types/type').AnalogResponseMessage} AnalogResponseMessage
 */

import { arraySplit, matchFeature, significantBytesToNumber } from '@/script/utils/utils';

export default [
  // firmwareName: 韌體名稱與版本
  {
    key: 'firmwareName',
    eventName: 'info',
    /**
     * @param {number[]} res 
     */
    matcher(res) {
      const featureBytes = [0xF0, 0x79];
      return matchFeature(res, featureBytes);
    },
    /**
     * @param {number[]} values 
     */
    getData(values) {
      // 取得特徵起點
      const index = values.lastIndexOf(0x79);

      const major = values[index + 1];
      const minor = values[index + 2];

      const nameBytes = values.slice(index + 3, -1);

      /** 在 D04 內容中可以知道 MSB 都是 0
       * 所以去除 0 之後，將剩下的 byte 都轉為字元後合併
       * 最後的結果就會是完整的名稱
       */
      const firmwareName = nameBytes
        .filter((byte) => byte !== 0)
        .map((byte) => String.fromCharCode(byte))
        .join('');

      return {
        ver: `${major}.${minor}`,
        firmwareName
      }
    },
  },

  // capabilitieResponse: 
  {
    key: 'capabilitieResponse',
    eventName: 'info',
    /**
     * @param {number[]} res 
     */
    matcher(res) {
      const featureBytes = [0xF0, 0x6C];
      return matchFeature(res, featureBytes);
    },
    /**
     * @param {number[]} valuesIn 
     */
    getData(valuesIn) {

      // 去除起始值、命令代號、結束值
      const values = valuesIn.filter((byte) =>
        ![0xF0, 0x6C, 0xF7].includes(byte)
      );

      // 根據分隔符號 0x7F 分割
      const pinParts = arraySplit(values, 0x7F);

      // 依序解析所有 pin
      const pins = pinParts.map((pinPart, index) => {
        // 每 2 個數值一組
        const modeParts = [];
        for (let i = 0; i < pinPart.length; i += 2) {
          modeParts.push(pinPart.slice(i, i + 2));
        }

        // 第一個數值為模式，第二個數值為解析度
        const capabilities = modeParts.map((modePart) => {
          const [mode, resolution] = modePart;
          return {
            mode, resolution
          }
        });

        return {
          number: index,
          capabilities,
        }
      });

      return {
        pins
      };
    },
  },

  // analogPinMappingResponse: 
  {
    key: 'analogPinMappingResponse',
    eventName: 'info',
    /**
     * @param {number[]} res 
     */
    matcher(res) {
      const featureBytes = [0xF0, 0x6A];
      return matchFeature(res, featureBytes);
    },
    /**
     * @param {number[]} values 
     */
    getData(values) {
      // 找到 6A 之 Index，從這裡開始往後找
      const index = values.findIndex(byte => byte === 0x6A);

      const bytes = values.slice(index + 1, -1);

      const analogPinMap = bytes.reduce((map, byte, index) => {
        // 127（0x7F）表示不支援類比功能
        if (byte !== 127) {
          map[index] = byte;
        }

        return map;
      }, {});

      return { analogPinMap };
    },
  },

  // digitalMessage: 數位訊息回應
  {
    key: 'digitalMessage',
    eventName: 'data:digitalMessage',
    /**
     * @param {number[]} res 
     */
    matcher(res) {
      const hasCmd = res.some((byte) => byte >= 0x90 && byte <= 0x9F);
      return hasCmd;
    },
    /**
     * @param {number[]} values 
     * @return {DigitalResponseMessage[]}
     */
    getData(values) {
      // 取得所有特徵點位置
      const indexs = values.reduce((acc, byte, index) => {
        if (byte >= 0x90 && byte <= 0x9F) {
          acc.push(index);
        }
        return acc;
      }, []);

      /** @type {DigitalResponseMessage[]} */
      const responses = indexs.reduce((acc, index) => {
        const bytes = values.slice(index + 1, index + 3);

        const port = values[index] - 0x90;
        const value = significantBytesToNumber(bytes);

        acc.push({
          port, value,
        });
        return acc;
      }, []);

      return responses;
    },
  },

  // analogMessage: 類比訊息回應
  {
    key: 'analogMessage',
    eventName: 'data:analogMessage',
    /**
     * @param {number[]} res 
     */
    matcher(res) {
      return res.some((byte) => byte >= 0xE0 && byte <= 0xEF);
    },
    /**
     * @param {number[]} values 
     * @return {AnalogResponseMessage[]}
     */
    getData(values) {
      // 取得所有特徵點位置
      const indexs = values.reduce((acc, byte, index) => {
        if (byte >= 0xE0 && byte <= 0xEF) {
          acc.push(index);
        }
        return acc;
      }, []);

      const analogVals = indexs.reduce((acc, index) => {
        const valueBytes = values.slice(index + 1, index + 3);

        const analogPin = values[index] - 0xE0;
        const value = significantBytesToNumber(valueBytes);

        acc.push({
          analogPin, value,
        });
        return acc;
      }, []);

      return analogVals;
    },
  },
]

