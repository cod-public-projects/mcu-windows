import { numberToSignificantBytes } from '@/script/utils/utils';

import { PinMode } from '@/script/utils/firmata.utils';
const { DIGITAL_INPUT, INPUT_PULLUP } = PinMode;

export default [
  // queryCapability: 查詢所有腳位與功能
  {
    key: 'queryCapability',
    getValue() {
      return [0xF0, 0x6B, 0xF7];
    },
  },

  // queryAnalogMapping: 查詢類比腳位映射
  {
    key: 'queryAnalogMapping',
    getValue() {
      return [0xF0, 0x69, 0xF7];
    },
  },

  // setMode: 設定腳位模式
  {
    key: 'setMode',
    getValue({ pin, mode }) {
      const cmds = [0xF4, pin, mode];

      // Mode 如果為 Digital Input，加入開啟自動回報命令
      if ([DIGITAL_INPUT, INPUT_PULLUP].includes(mode)) {
        const port = 0xD0 + ((pin / 8) | 0);
        cmds.push(port, 0x01);
      }

      return cmds;
    },
  },

  // setDigitalPinValue: 設定數位腳位數值
  {
    key: 'setDigitalPinValue',
    getValue({ pin, value }) {
      const level = value ? 0x01 : 0x00;
      return [0xF5, pin, level];
    },
  },

  // setPwmPinValue: 設定 PWM 數值
  {
    key: 'setPwmValue',
    getValue({ pin, value }) {
      const cmd = 0xE0 + pin;
      const [byte01, byte02] = numberToSignificantBytes(value);
      return [cmd, byte01, byte02];
    },
  },
]