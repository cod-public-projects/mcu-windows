/**
 * @typedef {Object} ResponseParseResult 回應資料解析結果
 * @property {string} key 回應 key
 * @property {string} eventName 事件名稱
 * @property {number[]} oriBytes 原始回應值
 * @property {Object} data 解析完成資料
 * 
 * @typedef {Object} PinInfo Pin 訊息
 * @property {number} number 腳位編號
 * @property {PinCapability[]} capabilities 特徵值
 * 
 * @typedef {Object} PinCapability Pin 功能
 * @property {number} mode 模式代號
 * @property {number} resolution 解析度
 * 
 * @typedef {Object} DigitalResponseMessage Digital 數值回應
 * @property {number} port Port 編號
 * @property {number} value 數值
 * 
 * @typedef {Object} AnalogResponseMessage Analog 數值回應
 * @property {number} analogPin 類比腳號（並非原始編號，需要比對類比腳位映射）
 * @property {number} value 數值
 */

import responsesDefines from '@/script/firmata/response-define';
import cmdsDefines from '@/script/firmata/cmd-define';

export default {
  /** 解析回應資料
   * @param {Number[]} res 接收數值
   */
  parseResponse(res) {
    // 找出符合回應
    const matchResDefines = responsesDefines.filter((define) =>
      define.matcher(res)
    );

    if (matchResDefines.length === 0) {
      return [];
    }

    const results = matchResDefines.map((resDefine) => {
      const data = resDefine.getData(res);
      const { key, eventName } = resDefine;

      /** @type {ResponseParseResult} */
      const result = {
        key,
        eventName,
        oriBytes: res,
        data,
      }
      return result;
    });

    return results;
  },

  /** 取得命令資料
   * @param {String} cmdKey 
   * @param {Object} params 
   * @return {Number[]}
   */
  getCmdBytes(cmdKey, params) {
    const target = cmdsDefines.find(({ key }) =>
      key === cmdKey
    );
    if (!target) {
      throw new Error(`${cmdKey} 命令不存在`);
    }

    return target.getValue(params);
  },
}
