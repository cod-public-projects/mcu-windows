const pinModeDefinition = [
  {
    code: 0x00,
    key: 'digitalInput',
    name: 'Digital Input',
    color: 'light-blue-3',
  },
  {
    code: 0x01,
    key: 'digitalOutput',
    name: 'Digital Output',
    color: 'cyan-3',
  },
  {
    code: 0x02,
    key: 'analogInput',
    name: 'Analog Input',
    color: 'red-4',
  },
  {
    code: 0x03,
    key: 'pwm',
    name: 'PWM',
    color: 'light-green-4',
  },
  {
    code: 0x04,
    key: 'servo',
    name: 'Servo',
    color: 'blue-5',
  },
  {
    code: 0x05,
    key: 'shift',
    name: 'Shift',
    color: 'purple-3',
  },
  {
    code: 0x06,
    key: 'i2c',
    name: 'I2C',
    color: 'green-4',
  },
  {
    code: 0x07,
    key: 'onewire',
    name: 'Onewire',
    color: 'indigo-4',
  },
  {
    code: 0x08,
    key: 'stepper',
    name: 'Stepper',
    color: 'lime-4',
  },
  {
    code: 0x09,
    key: 'encoder',
    name: 'Encoder',
    color: 'yellow-4',
  },
  {
    code: 0x0A,
    key: 'serial',
    name: 'Serial',
    color: 'amber-5',
  },

  {
    code: 0x0B,
    key: 'inputPullup',
    name: 'Input Pullup',
    color: 'teal-3',
  },
  {
    code: 0x0C,
    key: 'spi',
    name: 'SPI',
    color: 'amber-4',
  },
  {
    code: 0x0D,
    key: 'sonar',
    name: 'Sonar',
    color: 'orange-4',
  },
  {
    code: 0x0E,
    key: 'tone',
    name: 'Tone',
    color: 'deep-orange-4',
  },
  {
    code: 0x0F,
    key: 'dht',
    name: 'DHT',
    color: 'brown-3',
  },
];

export const PinMode = {
  /** 數位輸入 : 0x00 */
  DIGITAL_INPUT: 0x00,
  /** 數位輸出 : 0x01 */
  DIGITAL_OUTPUT: 0x01,
  /** 類比輸入 : 0x02 */
  ANALOG_INPUT: 0x02,
  /** PWM : 0x03 */
  PWM: 0x03,
  /** 數位上拉輸入 : 0x0B */
  INPUT_PULLUP: 0x0B,
}


export default {
  /** 根據模式數值取得資料
   * @param {Number} mode 模式數值
   */
  getDefineByCode(mode) {
    const target = pinModeDefinition.find((item) => item.code === mode);
    if (!target) {
      return null;
    }

    return target;
  },
};
