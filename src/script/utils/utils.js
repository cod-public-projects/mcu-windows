/** 根據指定元素分割矩陣
* separator 不會包含在矩陣中
* @param {Array} array
* @param {*} separator
*/
export function arraySplit(array, separator) {
  const allIndex = indexOfAll(array, separator);

  if (allIndex.length === 0) {
    return [array];
  }

  const initArray = [];

  const part = array.slice(0, allIndex[0]);
  initArray.push(part);

  const result = allIndex.reduce((acc, pos, index) => {
    const start = pos;
    const end = allIndex?.[index + 1] ?? null;

    // end 不存在表示為最後一個
    if (end === null) {
      const part = array.slice(start + 1);
      acc.push(part);
      return acc;
    }

    const part = array.slice(start + 1, end);
    acc.push(part);
    return acc;
  }, initArray);


  return result;
}

/** 取得指定元素在矩陣內所有 index
 * @param {Array} arr 
 * @param {*} val 
 * @return {Number[]}
 */
export function indexOfAll(array, val) {
  return array.reduce((acc, el, i) => (el === val ? [...acc, i] : acc), []);
}

/** 判斷 Array 包含另一特徵 Array
 * @param {Number[]} array 
 * @param {Number[]} feature 
 */
export function matchFeature(array, feature) {
  if (feature.length === 0) {
    return false;
  }

  const arrayString = array.join();
  const featureString = feature.join();

  return arrayString.includes(featureString);
}

/** 延遲指定毫秒
 * @param {number} millisecond 
 */
export function delay(millisecond) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), millisecond);
  });
}

/** 取得隨機長度字串
 * @param {number} len 指定字串長度
 * @param {String} [charSet] 指定組成字符
 * @return {string}
 */
export function getRandomString(len = 5, charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789') {
  let randomString = '';
  for (let i = 0; i < len; i++) {
    const randomPoz = Math.floor(Math.random() * charSet.length);
    randomString += charSet.substring(randomPoz, randomPoz + 1);
  }
  return randomString;
}


/** 將有效 Bytes 轉為數值
 * @param {number[]} bytes 有效位元矩陣。bytes[0] 為 LSB
 * @param {number} [bitsNum] 每 byte 有效位元數
 */
export function significantBytesToNumber(bytes, bitsNum = 7) {
  const number = bytes.reduce((acc, byte, index) => {
    const mesh = 2 ** bitsNum - 1;

    const validBits = byte & mesh;
    acc += (validBits << (bitsNum * index))

    return acc;
  }, 0);

  return number;
}

/** 將數值轉為 Bytes
 * @param {number} number 數值
 * @param {number} [length] bytes 數量
 * @param {number} [bitsNum] 每 byte 有效位元數
 */
export function numberToSignificantBytes(number, length = 2, bitsNum = 7) {
  const bytes = [];
  const mesh = 2 ** bitsNum - 1;

  let remainingValue = number;

  for (let i = 0; i < length; i++) {
    const byte = remainingValue & mesh;
    bytes.push(byte);

    remainingValue = remainingValue >> bitsNum;
  }

  return bytes;
}

/** 取得數值特定 Bit
 * @param {Number} number 
 * @param {Number} bitIndex bit Index。從最小位元並以 0 開始
 */
export function getBitWithNumber(number, bitIndex) {
  const mesh = 1 << bitIndex;
  const value = number & mesh;
  return !!value;
}


/** 根據區間映射數值
 * @param {Number} numberIn 待計算數值
 * @param {Number} inMin 輸入最小值
 * @param {Number} inMax 輸入最大值
 * @param {Number} outMin 輸出最小值
 * @param {Number} outMax 輸出最大值
 */
export function mapNumber(numberIn, inMin, inMax, outMin, outMax) {
  let number = numberIn;
  if (numberIn < inMin) {
    number = inMin;
  }
  if (numberIn > inMax) {
    number = inMax;
  }

  const result = (number - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
  return result;
}